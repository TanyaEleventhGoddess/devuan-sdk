#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# Importing libs
import os
import argparse
import devuan_sdk


def parse_args():
    parser = argparse.ArgumentParser(description='Devuan SDK')
    subparsers = parser.add_subparsers(
        dest='mode', help='modes', required=True)
    init = subparsers.add_parser('init', help='initializes structure')
    init.add_argument(
        '-d', '--debug', action='store_true', help='Enable debug mode')
    init.add_argument(
        '-q', '--quiet', action='store_true', help='Enable debug mode')
    return (parser)


def main():
    parser = parse_args()
    args = parser.parse_args()
    folder = devuan_sdk.BuildEnv(
        folder=os.environ.get('DISTRO_BUILD_FD')
        if os.environ.get('DISTRO_BUILD_FD') else os.getcwd(),
        debug=args.debug,
        quiet=args.quiet)
    getattr(folder, args.mode)()


try:
    main()
except KeyboardInterrupt:
    pass
