#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# Import libs
from typing import Optional, Union
import os
import apt
import apt_pkg
import subprocess
from apt.package import _file_is_same, UntrustedError


def check_untrust_pkg(src,
                      allow_unauthenticated: Optional[bool] = False,
                      *args,
                      **kwargs):
    if not (allow_unauthenticated or src.index.is_trusted):
        raise (UntrustedError(
            'Could not fetch {0} {1} source package: Source {2} is not trusted'
            .format(src.package, src.version, src.index.describe)))


def check_untrust_file(fil,
                       dpath,
                       allow_unauthenticated: Optional[bool] = False,
                       *args,
                       **kwargs):
    if not (allow_unauthenticated or fil.hashes.usable):
        raise (UntrustedError(
            'The item {0} could not be fetched: No trusted hash found.'.format(
                dpath)))


def download_src_pkg(self,
                     folder: str,
                     acq,
                     pkg,
                     progress=None,
                     unpack: Optional[bool] = True,
                     *args,
                     **kwargs):
    src = apt_pkg.SourceRecords()
    acq = acq if acq else apt_pkg.Acquire(
        progress or apt.progress.text.AcquireProgress())
    files = list()
    dsc = None
    if src.lookup(pkg):
        check_untrust_pkg(src, *args, **kwargs)
        for pkfile in src.files:
            path = os.path.basename(pkfile.path)
            dpath = '{0}/{1}'.format(folder, path)
            if _file_is_same(dpath, pkfile.size, pkfile.hashes):
                self.log.debug(
                    'Ignoring already existing file: {0}'.format(dpath))
                print('grrr')
                continue
            check_untrust_file(pkfile, dpath, *args, **kwargs)
            if pkfile.type == 'dsc':
                dsc = dpath
            files.append(
                apt_pkg.AcquireFile(
                    acq,
                    src.index.archive_uri(pkfile.path),
                    pkfile.hashes,
                    pkfile.size,
                    path,
                    destfile=dpath))
        acq.run()
        if dsc is None:
            raise (ValueError('No source for {0} {1}'.format(
                src.package, src.version)))
        if not unpack:
            return (os.path.abspath(dsc))
        outdir = '{0}/{1}-{2}'.format(folder, src.package,
                                      apt_pkg.upstream_version(src.version))
        if not os.path.isdir(outdir):
            subprocess.check_call(['dpkg-source', '-x', dsc, outdir])
        return (os.path.abspath(outdir))
    else:
        print(pkg)


def download_src_pkgs(self,
                      folder: str,
                      pkglist: Union[list, tuple],
                      progress=None,
                      *args,
                      **kwargs):
    acq = apt_pkg.Acquire(progress or apt.progress.text.AcquireProgress())
    for pkg in pkglist:
        download_src_pkg(
            self=self,
            folder=folder,
            acq=acq,
            pkg=pkg,
            progress=progress,
            *args,
            **kwargs)
