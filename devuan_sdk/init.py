#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# Import libs
import os
import subprocess
import shutil
from . import exceptions


def create_env(self, *args, **kwargs):
    for fd in ('sources', 'stage', 'chroot', 'builds'):
        fd = '{0}/{1}'.format(self.folder, fd)
        if not os.path.isdir(fd):
            os.mkdir(fd)


def dw_keyring(self, *args, **kwargs):
    fd = '{0}.gnupg'.format(self.folder)
    kring = '{0}/debian-keyring.gpg'.format(fd)
    if os.path.isfile(kring):
        self.log.info('Public keyring found')
        return (None)
    os.mkdir(fd)
    if not shutil.which('rsync'):
        raise (exceptions.UtilityNotFound('rsync'))
    subprocess.check_call([
        'rsync', '-az', '--progress',
        'keyring.debian.org::keyrings/keyrings/debian-keyring.gpg', fd
    ])
    self.log.info('GnuPG debian maintainers keyring succesfully imported')
