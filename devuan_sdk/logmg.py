#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# Import libraries
import sys
import logging


class ContextFilter(logging.Filter):
    def filter(self, record: logging.LogRecord) -> bool:
        if record.levelno > logging.INFO:
            record.emessage = '{0}: {1}'.format(record.levelname, record.msg)
        else:
            record.emessage = record.msg
        return (True)


class DefaultHandler(logging.StreamHandler):
    def flush(self):
        """
        Flushes the stream.
        """
        self.acquire()
        try:
            for stream in (sys.stdout, sys.stderr):
                if stream and hasattr(stream, "flush"):
                    stream.flush()
        finally:
            self.release()

    def emit(self, record):
        """
        Emit a record.
        If a formatter is specified, it is used to format the record.
        The record is then written to the stream with a trailing newline.  If
        exception information is present, it is formatted using
        traceback.print_exception and appended to the stream.  If the stream
        has an 'encoding' attribute, it is used to determine how to do the
        output to the stream.
        """
        try:
            msg = self.format(record)
            stream = sys.stdout if record.levelno < logging.ERROR else sys.stderr
            # issue 35046: merged two stream.writes into one.
            stream.write(msg + self.terminator)
            self.flush()
        except RecursionError:    # See issue 36272
            raise
        except Exception:
            self.handleError(record)
