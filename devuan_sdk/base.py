#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# Import libraries
from typing import Optional
import os
import logging
from . import init, apt, logmg


class BuildEnv:
    def __init__(self,
                 folder: Optional[str] = None,
                 debug: Optional[bool] = False,
                 quiet: Optional[bool] = False,
                 *args,
                 **kwargs):
        self.folder = folder
        self.log = logging.getLogger(self.lfolder)
        self.log.setLevel(
            logging.WARN if quiet else (
                logging.DEBUG if debug else logging.INFO))
        handler = logmg.DefaultHandler()
        handler.setLevel(
            logging.WARN if quiet else (
                logging.DEBUG if debug else logging.INFO))
        handler.addFilter(logmg.ContextFilter())
        handler.setFormatter(logging.Formatter('%(emessage)s'))
        self.log.addHandler(handler)

    @property
    def folder(self):
        if '_folder' in self.__dict__:
            return (self._folder)
        return (None)

    @folder.setter
    def folder(self, value):
        if isinstance(value, str) and os.path.isdir(value):
            self._folder = value

    @property
    def lfolder(self):
        if self.folder and '/' in self.folder and not len(
                self.folder.split('/')) < 2:
            return (self.folder[self.folder.rindex('/') + 1:])
        return (self.folder)

    def init(self, *args, **kwargs):
        init.create_env(self, *args, **kwargs)
        apt.download_src_pkgs(
            self,
            folder='{0}/sources'.format(self.folder),
            pkglist=('debootstrap', 'devuan-baseconf', 'devuan-keyring',
                     'debian-keyring'))
        init.dw_keyring(self, *args, **kwargs)
        self.log.info('Devuan SDK succesfully initialized')
