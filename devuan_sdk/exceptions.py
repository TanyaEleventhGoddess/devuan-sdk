#!/usr/bin/env python3
# -*- coding:utf-8 -*-


class UtilityNotFound(Exception):
    def __init__(self, util: str):
        super().__init__('Utility not found: {0}'.format(util))
