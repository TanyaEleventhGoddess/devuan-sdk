# Devuan GNU/Linux

## Simple (package) development kit

### version 0.1

#### DISCLAIMER

This SDK version is not up to date with the Continuous Integration
workflow Devuan has in place since a few months now. Updates to this
SDK will follow, meanwhile be aware that this code will not produce
iso images valid for release, since these are done via CI now.

### Introduction

This set of scripts aid package maintainers to import sources from
Debian, verify signatures and stage them to be imported inside
Devuan's git repository.

The Devuan SDK is a fresh take to old tasks :^) acting as a sort of
interactive shell extension. All the instructions below should be
followed while already running in ZSh. A clear advantage is having tab
completion on commands, when running it interactively.

BEWARE this is still in development and does not addresses strictly
security issues nor wrong usage. USE AT YOUR OWN RISK and in any case
DON'T USE ON YOUR PERSONAL MACHINE.
If you try this fast and loose use a disposable system ;^)
